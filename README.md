# HTML/CSS Lab tasks
## Task 1 (Float)
#### Usefull links:
1. https://habrahabr.ru/post/142486/
2. https://webref.ru/course/positioning/float
3. https://webref.ru/layout/advanced-html-css/detailed-css-positioning (read until position :) )

#### What should you do:

1. Create 10 blocks and make them go side by side using "float". Space between blocks should be 10px. Width of each block is 20% and height is 100px. Number inside a block should be centered vertically and horizontally.

![](task1(Float)/task1_1.PNG)

2. Create two column layout using float. The first column should have 200px width and the second should be flexible (width is not specified).

![](task1(Float)/task1_2.PNG)

3. Create three column layout using float. The first column should have 200px width, the second should be flexible and the last one should have 100px width.

![](task1(Float)/task1_3.PNG)

4. Create header which contains navigation with 3 items. The items should go side by side. Also, navigation should be from the right side of the header.

![](task1(Float)/task1_4.PNG)


## Task 2 (Position)

#### Usefull links
1. https://learn.javascript.ru/position
2. https://webref.ru/course/positioning/position
3. https://webref.ru/css/position

#### What should you do

1. Create notification bar: envelop with the count of unread messages.

![](task2(Position)/task2_1.PNG)

Envelop icon you can use is placed in task2(Position)/Envelop.svg

2. Create slider (static without any behaviuor) using positioning.

![](task2(Position)/task2_2.PNG)

3. Create dialog window to communicate with customers. It should be aligned to the right/bottom corner of the window and it should be always there independent of scrolling.

![](task2(Position)/task2_3.PNG)

## Task3

[task3 layout](task3/task3.psd)

There is a psd layout. You should develop the page according to this layout.

#### Recommendations

1. Use different folders for source files: "task1/styles/styles.css", "task1/icons/...", "task1/images/...". index.html should be in the root folder.
2. DON'T USE FLEXBOX!
3. Layout should look good for a desktop. It is no required to implement responsive for this task
3. Use html tags such (main, header, footer, nav, etc.)
4. Use line-height to align one line of text vertically
5. You can also use position or pseudo element to align block vertically.
6. Use box-sizing where it is necessary
7. Use float or display: inline-block to place blocks in a single row.
8. Use opacity property and :hover pseudo class to create the blue overlay with icons over the image.

## Task4

[task4 layout](task4/task4.psd)

There is a psd layout. You should develop the page according to this layout.

#### Recommendations

1. Use different folders for source files: "task1/styles/styles.css", "task1/icons/...", "task1/images/...". index.html should be in the root folder.
2. DON'T USE FLEXBOX!
3. Use html tags such (main, header, footer, nav, etc.)
4. Use line-height to align one line of text vertically
5. You can use pseudo element to align block vertically or positions.
6. Use box-sizing where it is necessary
7. Use float or display: inline-block to place blocks in a single row.
8. If you can align smth not using position, do it.
9. Center content horizontally using text-align for inline or inline-block elemtns and margin: 0 auto for block elements. Don't set fixed margins for horizontal alignment.
10. Think about content. If you think that content can be changed (text, for example, can become bigger), then be sure that you markup will not be broken.


